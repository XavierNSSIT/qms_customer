﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using QMS_CS.Helpers;

namespace QMS_CS.Models
{

    public class ScreenInfoModel
    {
        public List<ZoneCounterType> model { get; set; }
    }

    public class ZoneCounterType
    {
        public MZonesShort Zone { get; set; }
        public List<MTypesShort> Types { get; set; }
    }

    public class MZonesShort
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }

    public class MTypesShort
    {
        public string Id { get; set; }
        public string Description { get; set; }
    }

    public class TypesByZone
    {
        public string MZones_Id { get; set; }
        public string MZones_Description { get; set; }
        public string MTypes_Id { get; set; }
        public string MTypes_Description { get; set; }
    }

    public class QueueNumbers
    {
        DateTime _Time;
        public string QueueNo { get; set; }
        public string Type { get; set; }
        public string CurrentServingNo { get; set; }
        public virtual DateTime Time
        {
            get { return _Time; }
            set { _Time = value; }
        }
        public QueueNumbers()
        {

            DateTime now = DateTimeHelper.GetNow();
            Time = now;
        }
        public string DriverName { get; set; }
        public string PlateNo { get; set; }
    }

    public class DriverInfo
    {
        public string DriverName { get; set; }
        public string Plateno { get; set; }
    }

    public class TransactionReportModel
    {
        public string QueueNo { get; set; }
        public string Type { get; set; }
        public string ServedFlag { get; set; }
        public string CounterServed { get; set; }
        public DateTime NoGeneratedTime { get; set; }
        public DateTime NoServedTime { get; set; }
        public string DriverName { get; set; }
        public string DriverPlateNo { get; set; }
    }

    public class TransactionReportOutput
    {
        public string QueueNo { get; set; }
        public string Type { get; set; }
        public string ServedFlag { get; set; }
        public string CounterServed { get; set; }
        public string NoGeneratedTime { get; set; }
        public string NoServedTime { get; set; }
        public string WaitTime { get; set; }
        public string DriverName { get; set; }
        public string DriverPlateNo { get; set; }
    }
}
