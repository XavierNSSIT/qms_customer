﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QMS_CS.Models;
using System.Drawing.Printing;
using System.Drawing.Text;
using System.Resources;
using QMS_CS.btnProduct;
using System.Web;
using System.Net.Http;
using Newtonsoft.Json;
using System.Configuration;

namespace QMS_CS
{
    public partial class MainForm : Form
    {
        private static string apiIp = ConfigurationManager.AppSettings["APIIP"].ToString();
        //private string[] queueType = {"Special", "OKU", "Senior", "Normal", "Advanced"};
        
        private IList<ZoneCounterType> screenModels = new List<ZoneCounterType>();
        private ZoneCounterType screenModel = null;
        private string slipType = "";
        
        private int winHeight = 0;
        private int winWidth = 0;
        public static string typeFromMain = "";


        public MainForm()
        {
            //FormBorderStyle = FormBorderStyle.None;
            //WindowState = FormWindowState.Maximized;

            GetMainScreenInfo();
            InitializeComponent();
            tmrTime.Start();
        }

        private void CreatePanel()
        {
            //System.Drawing.Rectangle workingRectangle = Screen.main .WorkingArea;
            //winHeight = workingRectangle.Height;
            winHeight = mainPanel.Height;
            //winWidth = workingRectangle.Width;
            winWidth = mainPanel.Width;

            mainPanel.Size = new Size(winWidth - 10, winHeight-65);

            int typeCount = screenModels[0].Types.Count;
            double typeCountDouble = Convert.ToDouble(typeCount);
            double buttonWidth = 250;
            double buttonHeight = 80;
            double spacingBtwnButton = 15;
            double spacingBtwnButtonSide = 30;
            double spaceOccupied;
            double top = 0;
            double left = 0;
            int oddCount = 0;
            double topOld = 0;
            left = (winWidth - buttonWidth) / 2;

            for (int i = 0; i < typeCount; i++)
            {
                if (typeCount > 5)
                {
                    spaceOccupied = (buttonHeight * Math.Ceiling((typeCountDouble / 2))) + (spacingBtwnButton) * (typeCount - 1);
                    top = (winHeight - 45 - spaceOccupied) / 2;

                    if (i % 2 == 0)
                    {
                        left = (winWidth - (buttonWidth * 2) - spacingBtwnButtonSide) / 2;
                        topOld = top;
                        if (i != 0)
                        {
                            oddCount++;
                            top += (buttonHeight + spacingBtwnButton) * (i - oddCount);
                            topOld = top;
                        }
                    }
                    else
                    {
                        top = topOld;
                        left = ((winWidth - (buttonWidth * 2) - spacingBtwnButtonSide) / 2) + buttonWidth + spacingBtwnButtonSide;
                    }
                }
                else
                {
                    spaceOccupied = (buttonHeight * typeCountDouble) + (spacingBtwnButton) * (typeCount - 1);
                    top = (winHeight - 38 - spaceOccupied) / 2;
                    if (i != 0)
                    {
                        top += (buttonHeight + spacingBtwnButton) * i;
                    }
                }
                

                Button_WOC btn = new Button_WOC();
                btn.Left = Convert.ToInt32(left);
                btn.Top = Convert.ToInt32(top);
                btn.Text = screenModels[0].Types[i].Description;
                btn.Name = screenModels[0].Types[i].Id;
                btn.FlatAppearance.BorderSize = 0;
                btn.FlatStyle = FlatStyle.Flat;
                btn.ButtonColor = Color.FromArgb(11, 23, 30);
                btn.TextColor = Color.White;
                btn.Size = new Size(Convert.ToInt32(buttonWidth) - 10, Convert.ToInt32(buttonHeight) - 10);
                btn.Font = new Font("Cooper Black", 25, FontStyle.Bold);
                btn.ForeColor = Color.White;
                btn.OnHoverBorderColor = Color.White;
                btn.OnHoverButtonColor = Color.White;
                btn.OnHoverTextColor = Color.FromArgb(11, 23, 30);
                btn.Click += (obj, eArgs) =>
                {
                    //slipType = btn.Name;

                    FormDriverInfo InfoForm = new FormDriverInfo();
                    typeFromMain = btn.Name;
                    //InfoForm.MdiParent = MainForm.ActiveForm;
                    InfoForm.ShowDialog();

                    //GetSlipInfo();
                };
                mainPanel.Controls.Add(btn);
            }
        }

        

        public IEnumerable<Control> GetAll(Control control, Type type)
        {
            var controls = control.Controls.Cast<Control>();

            return controls.SelectMany(ctrl => GetAll(ctrl, type))
                                      .Concat(controls)
                                      .Where(c => c.GetType() == type);
        }

        

        private async void GetMainScreenInfo()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://" + apiIp);
            HttpResponseMessage response = await client.GetAsync("/QMS_API/api/CustServ/GetMainScreen");
            string result = await response.Content.ReadAsStringAsync();

            log4net.LogManager.GetLogger("XML").Info("Get Main Screen : ");
            log4net.LogManager.GetLogger("XML").Info(result);

            var model = JsonConvert.DeserializeObject<IList<ZoneCounterType>>(result);

            for (int i = 0; i < model.Count; i++)
            {
                screenModel = new ZoneCounterType()
                {
                    Zone = model[i].Zone,
                    Types = model[i].Types
                };

                screenModels.Add(screenModel);
            }

            CreatePanel();
        }

        

        private void tmrTime_Tick(object sender, EventArgs e)
        {
            DateTime timeNow = DateTime.Now;
            lblDate.Text = timeNow.ToString("dd-MMM-yyyy");
            lblTime.Text = timeNow.ToString("hh:mm:sss:tt");

            lblDate.Left = winWidth - lblDate.Size.Width;
            lblTime.Left = winWidth - lblTime.Size.Width - 2;
        }

        private void btnReport_Click(object sender, EventArgs e)
        {
            FormReport report = new FormReport();
            report.ShowDialog();
        }
    }
}
