﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QMS_CS.Models;
using System.Drawing.Printing;
using System.Drawing.Text;
using System.Resources;
using QMS_CS.btnProduct;
using System.Web;
using System.Net.Http;
using Newtonsoft.Json;
using System.Configuration;
using QMS_CS.Helpers;
using Microsoft.Reporting.WinForms;


namespace QMS_CS
{
    public partial class FormReport : Form
    {
        private static string apiIp = ConfigurationManager.AppSettings["APIIP"].ToString();
        public FormReport()
        {
            InitializeComponent();
        }

        private void FormReport_Load(object sender, EventArgs e)
        {
            cbDate.SelectedIndex = 0;
            this.reportViewer1.RefreshReport();
            this.reportViewer1.RefreshReport();
            this.reportViewer1.RefreshReport();
        }

        private void btnCancelReport_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void cbDate_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (cbDate.SelectedIndex == 1)
            {
                lblDateFrom.Visible = true;
                lblDateTo.Visible = true;
                dtpDateFrom.Visible = true;
                dtpDateTo.Visible = true;
                dtpDateFrom.Focus();
            }
            else
            {
                lblDateFrom.Visible = false;
                lblDateTo.Visible = false;
                dtpDateFrom.Visible = false;
                dtpDateTo.Visible = false;
                btnGenerateReport.Focus();
                dtpDateFrom.Value = DateTime.Now;
                dtpDateTo.Value = DateTime.Now;
            }
        }

        private void btnGenerateReport_Click(object sender, EventArgs e)
        {
            GenerateReportData();
        }

        private async void GenerateReportData()
        {
            DateTime fromDate = dtpDateFrom.Value;
            string dateFrom = DateTimeHelper.ToDateOnlyFormat(fromDate);

            DateTime toDate = dtpDateTo.Value;
            string dateTo = DateTimeHelper.ToDateOnlyFormat(toDate);

            IList<TransactionReportOutput> outputs = new List<TransactionReportOutput>();
            TransactionReportOutput output = null;

            var client = new HttpClient();
            client.BaseAddress = new Uri("http://" + apiIp);
            HttpResponseMessage response = await client.GetAsync("/QMS_API/api/CustServ/GetTxnReport?dateFrom=" + dateFrom + "&dateTo=" + dateTo);
            string result = await response.Content.ReadAsStringAsync();

            log4net.LogManager.GetLogger("XML").Info("Get Report Data : ");
            log4net.LogManager.GetLogger("XML").Info(result);

            var model = JsonConvert.DeserializeObject<IList<TransactionReportModel>>(result);

            for (int i = 0; i < model.Count; i++)
            {
                output = new TransactionReportOutput()
                {
                    QueueNo = model[i].QueueNo,
                    Type = model[i].Type,
                    CounterServed = model[i].CounterServed,
                    NoGeneratedTime = DateTimeHelper.ToDateTimeFormat(model[i].NoGeneratedTime),
                    DriverName = model[i].DriverName,
                    DriverPlateNo = model[i].DriverPlateNo
                };

                if (model[i].ServedFlag == "0")
                {
                    output.ServedFlag = "Not Served";
                    output.NoServedTime = "-";
                    output.WaitTime = "-";
                }
                else
                {
                    output.ServedFlag = "Served";
                    output.NoServedTime = DateTimeHelper.ToDateTimeFormat(model[i].NoServedTime);
                    output.WaitTime = (model[i].NoServedTime - model[i].NoGeneratedTime).ToString();
                }

                

                outputs.Add(output);
            }          

            LocalReport localReport = reportViewer1.LocalReport;
            localReport.ReportPath = "Report1.rdlc";

            ReportDataSource reportDataSource = new ReportDataSource();
            reportDataSource.Name = "Transactions";
            reportDataSource.Value = outputs;
            localReport.DataSources.Clear();
            localReport.DataSources.Add(reportDataSource);
            reportViewer1.RefreshReport();
            
        }

    }
}
