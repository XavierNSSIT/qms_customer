﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using QMS_CS.Models;
using System.Drawing.Printing;
using System.Drawing.Text;
using System.Resources;
using QMS_CS.btnProduct;
using System.Web;
using System.Net.Http;
using Newtonsoft.Json;
using System.Configuration;

namespace QMS_CS
{
    public partial class FormDriverInfo : Form
    {
        private static string apiIp = ConfigurationManager.AppSettings["APIIP"].ToString();
        private QueueNumbers qNumber = null;

        public FormDriverInfo()
        {
            InitializeComponent();
        }

        private void FormDriverInfo_Load(object sender, EventArgs e)
        {
            lblType.Text = MainForm.typeFromMain;
        }

        private void btnDInfoCancel_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnDInfoConfirm_Click(object sender, EventArgs e)
        {
            GetSlipInfo();
            this.Close();
        }
        private void CreateReceipt(object sender, System.Drawing.Printing.PrintPageEventArgs e)
        {
            string copy = sender as string;

            Graphics graphic = e.Graphics;
            Rectangle rect1 = new Rectangle(10, 0, 270, 202);

            StringFormat sf = new StringFormat();
            sf.Alignment = StringAlignment.Center;

            StringFormat sfFar = new StringFormat();
            sfFar.LineAlignment = StringAlignment.Far;
            sfFar.Alignment = StringAlignment.Far;

            StringFormat sfNear = new StringFormat();
            sfNear.LineAlignment = StringAlignment.Near;
            sfNear.Alignment = StringAlignment.Near;

            //copies
            Rectangle rectCopy = new Rectangle(10, 0, 140, 20);
            graphic.DrawString(copy, new Font("Arial", 10), new SolidBrush(Color.Black), rectCopy, sfNear);

            Rectangle rectImage = new Rectangle(130, 0, 140, 40);
            Bitmap image = new Bitmap(QMS_CS.Resource1.KLIA2);

            graphic.DrawImage(image, rectImage, new Rectangle(0, 0, 140, 40), GraphicsUnit.Pixel);

            int startX = 10;
            int startY = 10;
            int offset = 25;

            graphic.DrawString("______________________________________", new Font("Arial", 12), new SolidBrush(Color.Black), startX, startY + offset);

            offset += 30;
            Rectangle recType = new Rectangle(10, offset, 270, 28);
            graphic.DrawString(qNumber.Type, new Font("Arial", 20, FontStyle.Bold), new SolidBrush(Color.Black), recType, sf);

            offset += 30;
            Rectangle rectNmbr = new Rectangle(10, offset, 270, 35);
            graphic.DrawString(AddSpacingBtwnString(qNumber.QueueNo), new Font("Arial", 26, FontStyle.Bold), new SolidBrush(Color.Black), rectNmbr, sf);

            offset += 45;
            Rectangle recName = new Rectangle(10, offset, 270, 15);
            graphic.DrawString(qNumber.DriverName, new Font("Arial", 10, FontStyle.Regular), new SolidBrush(Color.Black), recName, sfNear);

            offset += 25;
            Rectangle recPlate = new Rectangle(10, offset, 270, 18);
            graphic.DrawString(qNumber.PlateNo, new Font("Arial", 12, FontStyle.Bold), new SolidBrush(Color.Black), recPlate, sfFar);

            offset += 25;
            Rectangle rectNS = new Rectangle(10, offset + 6, 270, 15);
            graphic.DrawString("Now Serving : ", new Font("Arial", 8), new SolidBrush(Color.Black), rectNS, sfNear);
            graphic.DrawString(AddSpacingBtwnString(qNumber.CurrentServingNo), new Font("Arial", 14, FontStyle.Bold), new SolidBrush(Color.Black), 80, offset);

            Rectangle rectTime = new Rectangle(10, 0, 270, 190);
            graphic.DrawString("Printed At : " + qNumber.Time.ToString("HH:mm:tt"), new Font("Arial", 6), new SolidBrush(Color.Black), rectTime, sfFar);
            offset = offset + 15;
            graphic.DrawString(qNumber.Time.ToString("dd/MM/yyyy"), new Font("Arial", 6), new SolidBrush(Color.Black), rect1, sfFar);
        }

        private string AddSpacingBtwnString(string String)
        {
            string stringWithSpace = "";

            for (int i = 0; i < String.Length; i++)
            {
                stringWithSpace += String[i] + " ";

                if (i == String.Length - 1)
                {
                    stringWithSpace = stringWithSpace.Substring(0, stringWithSpace.Length - 1);
                }
            }

            return stringWithSpace;
        }   
        private void PrintSlip(string copy)
        {
            PrinterSettings settings = new PrinterSettings();

            PrintDocument printDocument = new PrintDocument();

            printDocument.PrinterSettings.PrinterName = settings.PrinterName;

            //printDocument.PrintPage += new System.Drawing.Printing.PrintPageEventHandler(CreateReceipt);
            printDocument.PrintPage += (sender, args) => CreateReceipt(copy, args);

            printDocument.Print();
        }

        private async void GetSlipInfo()
        {
            var client = new HttpClient();
            client.BaseAddress = new Uri("http://" + apiIp);
            HttpResponseMessage response = await client.GetAsync("/QMS_API/api/CustServ/GetQueueNo?type=" + lblType.Text + "&driverName=" + txtDriverName.Text + "&plateNo=" + txtPlateNo.Text);
            string result = await response.Content.ReadAsStringAsync();

            log4net.LogManager.GetLogger("XML").Info("Get Slip Info : \n");
            log4net.LogManager.GetLogger("XML").Info(result);

            var model = JsonConvert.DeserializeObject<QueueNumbers>(result);

            qNumber = new QueueNumbers()
            {
                QueueNo = model.QueueNo,
                Type = model.Type,
                CurrentServingNo = model.CurrentServingNo,
                Time = model.Time,
                DriverName = model.DriverName,
                PlateNo = model.PlateNo
            };

            PrintSlip("Driver Copy");
            PrintSlip("Office Copy");
        }
    }
}
