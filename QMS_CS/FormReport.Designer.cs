﻿namespace QMS_CS
{
    partial class FormReport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.dtpDateFrom = new System.Windows.Forms.DateTimePicker();
            this.cbDate = new System.Windows.Forms.ComboBox();
            this.dtpDateTo = new System.Windows.Forms.DateTimePicker();
            this.lblDateFrom = new System.Windows.Forms.Label();
            this.lblDateTo = new System.Windows.Forms.Label();
            this.btnGenerateReport = new System.Windows.Forms.Button();
            this.btnCancelReport = new System.Windows.Forms.Button();
            this.reportViewer1 = new Microsoft.Reporting.WinForms.ReportViewer();
            this.DS_Transactions = new QMS_CS.DS_Transactions();
            this.TransactionsBindingSource = new System.Windows.Forms.BindingSource(this.components);
            this.TransactionReportBindingSource = new System.Windows.Forms.BindingSource(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.DS_Transactions)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionsBindingSource)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionReportBindingSource)).BeginInit();
            this.SuspendLayout();
            // 
            // dtpDateFrom
            // 
            this.dtpDateFrom.CustomFormat = "dd-MMM-yyyy";
            this.dtpDateFrom.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateFrom.Location = new System.Drawing.Point(267, 11);
            this.dtpDateFrom.Name = "dtpDateFrom";
            this.dtpDateFrom.Size = new System.Drawing.Size(100, 20);
            this.dtpDateFrom.TabIndex = 0;
            this.dtpDateFrom.Visible = false;
            // 
            // cbDate
            // 
            this.cbDate.FormattingEnabled = true;
            this.cbDate.Items.AddRange(new object[] {
            "Today",
            "Previous Day"});
            this.cbDate.Location = new System.Drawing.Point(4, 11);
            this.cbDate.Name = "cbDate";
            this.cbDate.Size = new System.Drawing.Size(165, 21);
            this.cbDate.TabIndex = 1;
            this.cbDate.SelectedIndexChanged += new System.EventHandler(this.cbDate_SelectedIndexChanged);
            // 
            // dtpDateTo
            // 
            this.dtpDateTo.CustomFormat = "dd-MMM-yyyy";
            this.dtpDateTo.Format = System.Windows.Forms.DateTimePickerFormat.Custom;
            this.dtpDateTo.Location = new System.Drawing.Point(440, 11);
            this.dtpDateTo.Name = "dtpDateTo";
            this.dtpDateTo.Size = new System.Drawing.Size(100, 20);
            this.dtpDateTo.TabIndex = 2;
            this.dtpDateTo.Visible = false;
            // 
            // lblDateFrom
            // 
            this.lblDateFrom.AutoSize = true;
            this.lblDateFrom.Location = new System.Drawing.Point(199, 14);
            this.lblDateFrom.Name = "lblDateFrom";
            this.lblDateFrom.Size = new System.Drawing.Size(62, 13);
            this.lblDateFrom.TabIndex = 3;
            this.lblDateFrom.Text = "Date From :";
            this.lblDateFrom.Visible = false;
            // 
            // lblDateTo
            // 
            this.lblDateTo.AutoSize = true;
            this.lblDateTo.Location = new System.Drawing.Point(373, 14);
            this.lblDateTo.Name = "lblDateTo";
            this.lblDateTo.Size = new System.Drawing.Size(52, 13);
            this.lblDateTo.TabIndex = 4;
            this.lblDateTo.Text = "Date To :";
            this.lblDateTo.Visible = false;
            // 
            // btnGenerateReport
            // 
            this.btnGenerateReport.Location = new System.Drawing.Point(616, 3);
            this.btnGenerateReport.Name = "btnGenerateReport";
            this.btnGenerateReport.Size = new System.Drawing.Size(75, 34);
            this.btnGenerateReport.TabIndex = 5;
            this.btnGenerateReport.Text = "Generate";
            this.btnGenerateReport.UseVisualStyleBackColor = true;
            this.btnGenerateReport.Click += new System.EventHandler(this.btnGenerateReport_Click);
            // 
            // btnCancelReport
            // 
            this.btnCancelReport.Location = new System.Drawing.Point(698, 3);
            this.btnCancelReport.Name = "btnCancelReport";
            this.btnCancelReport.Size = new System.Drawing.Size(75, 34);
            this.btnCancelReport.TabIndex = 6;
            this.btnCancelReport.Text = "Cancel";
            this.btnCancelReport.UseVisualStyleBackColor = true;
            this.btnCancelReport.Click += new System.EventHandler(this.btnCancelReport_Click);
            // 
            // reportViewer1
            // 
            this.reportViewer1.Location = new System.Drawing.Point(4, 43);
            this.reportViewer1.Name = "reportViewer1";
            this.reportViewer1.Size = new System.Drawing.Size(1007, 521);
            this.reportViewer1.TabIndex = 7;
            // 
            // DS_Transactions
            // 
            this.DS_Transactions.DataSetName = "DS_Transactions";
            this.DS_Transactions.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema;
            // 
            // TransactionsBindingSource
            // 
            this.TransactionsBindingSource.DataMember = "Transactions";
            this.TransactionsBindingSource.DataSource = this.DS_Transactions;
            // 
            // TransactionReportBindingSource
            // 
            this.TransactionReportBindingSource.DataSource = typeof(QMS_CS.Models.TransactionReportOutput);
            // 
            // FormReport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1012, 565);
            this.Controls.Add(this.reportViewer1);
            this.Controls.Add(this.btnCancelReport);
            this.Controls.Add(this.btnGenerateReport);
            this.Controls.Add(this.lblDateTo);
            this.Controls.Add(this.lblDateFrom);
            this.Controls.Add(this.dtpDateTo);
            this.Controls.Add(this.cbDate);
            this.Controls.Add(this.dtpDateFrom);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Fixed3D;
            this.Name = "FormReport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Report";
            this.Load += new System.EventHandler(this.FormReport_Load);
            ((System.ComponentModel.ISupportInitialize)(this.DS_Transactions)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionsBindingSource)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TransactionReportBindingSource)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.DateTimePicker dtpDateFrom;
        private System.Windows.Forms.ComboBox cbDate;
        private System.Windows.Forms.DateTimePicker dtpDateTo;
        private System.Windows.Forms.Label lblDateFrom;
        private System.Windows.Forms.Label lblDateTo;
        private System.Windows.Forms.Button btnGenerateReport;
        private System.Windows.Forms.Button btnCancelReport;
        private System.Windows.Forms.BindingSource TransactionReportBindingSource;
        private System.Windows.Forms.BindingSource TransactionsBindingSource;
        private DS_Transactions DS_Transactions;
        private Microsoft.Reporting.WinForms.ReportViewer reportViewer1;

    }
}