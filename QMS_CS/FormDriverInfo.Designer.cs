﻿namespace QMS_CS
{
    partial class FormDriverInfo
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.lblType = new System.Windows.Forms.Label();
            this.btnDInfoCancel = new QMS_CS.btnProduct.Button_WOC();
            this.btnDInfoConfirm = new QMS_CS.btnProduct.Button_WOC();
            this.txtPlateNo = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.txtDriverName = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.panel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(215)))), ((int)(((byte)(255)))));
            this.panel1.Controls.Add(this.lblType);
            this.panel1.Controls.Add(this.btnDInfoCancel);
            this.panel1.Controls.Add(this.btnDInfoConfirm);
            this.panel1.Controls.Add(this.txtPlateNo);
            this.panel1.Controls.Add(this.label2);
            this.panel1.Controls.Add(this.txtDriverName);
            this.panel1.Controls.Add(this.label1);
            this.panel1.Location = new System.Drawing.Point(1, 2);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(298, 213);
            this.panel1.TabIndex = 0;
            // 
            // lblType
            // 
            this.lblType.AutoSize = true;
            this.lblType.Location = new System.Drawing.Point(20, 63);
            this.lblType.Name = "lblType";
            this.lblType.Size = new System.Drawing.Size(35, 13);
            this.lblType.TabIndex = 14;
            this.lblType.Text = "label3";
            this.lblType.Visible = false;
            // 
            // btnDInfoCancel
            // 
            this.btnDInfoCancel.BorderColor = System.Drawing.Color.Black;
            this.btnDInfoCancel.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(215)))), ((int)(((byte)(255)))));
            this.btnDInfoCancel.FlatAppearance.BorderSize = 0;
            this.btnDInfoCancel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDInfoCancel.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDInfoCancel.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDInfoCancel.Location = new System.Drawing.Point(147, 151);
            this.btnDInfoCancel.Name = "btnDInfoCancel";
            this.btnDInfoCancel.OnHoverBorderColor = System.Drawing.Color.Black;
            this.btnDInfoCancel.OnHoverButtonColor = System.Drawing.Color.Black;
            this.btnDInfoCancel.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(215)))), ((int)(((byte)(255)))));
            this.btnDInfoCancel.Size = new System.Drawing.Size(120, 45);
            this.btnDInfoCancel.TabIndex = 13;
            this.btnDInfoCancel.Text = "Cancel";
            this.btnDInfoCancel.TextColor = System.Drawing.Color.Black;
            this.btnDInfoCancel.UseVisualStyleBackColor = true;
            this.btnDInfoCancel.Click += new System.EventHandler(this.btnDInfoCancel_Click);
            // 
            // btnDInfoConfirm
            // 
            this.btnDInfoConfirm.BorderColor = System.Drawing.Color.Black;
            this.btnDInfoConfirm.ButtonColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(215)))), ((int)(((byte)(255)))));
            this.btnDInfoConfirm.FlatAppearance.BorderSize = 0;
            this.btnDInfoConfirm.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnDInfoConfirm.Font = new System.Drawing.Font("Cooper Black", 14.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnDInfoConfirm.ForeColor = System.Drawing.SystemColors.ControlText;
            this.btnDInfoConfirm.Location = new System.Drawing.Point(21, 151);
            this.btnDInfoConfirm.Name = "btnDInfoConfirm";
            this.btnDInfoConfirm.OnHoverBorderColor = System.Drawing.Color.Black;
            this.btnDInfoConfirm.OnHoverButtonColor = System.Drawing.Color.Black;
            this.btnDInfoConfirm.OnHoverTextColor = System.Drawing.Color.FromArgb(((int)(((byte)(201)))), ((int)(((byte)(215)))), ((int)(((byte)(255)))));
            this.btnDInfoConfirm.Size = new System.Drawing.Size(120, 45);
            this.btnDInfoConfirm.TabIndex = 12;
            this.btnDInfoConfirm.Text = "Confirm";
            this.btnDInfoConfirm.TextColor = System.Drawing.Color.Black;
            this.btnDInfoConfirm.UseVisualStyleBackColor = true;
            this.btnDInfoConfirm.Click += new System.EventHandler(this.btnDInfoConfirm_Click);
            // 
            // txtPlateNo
            // 
            this.txtPlateNo.Location = new System.Drawing.Point(20, 105);
            this.txtPlateNo.Name = "txtPlateNo";
            this.txtPlateNo.Size = new System.Drawing.Size(262, 20);
            this.txtPlateNo.TabIndex = 11;
            this.txtPlateNo.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label2.ForeColor = System.Drawing.Color.Black;
            this.label2.Location = new System.Drawing.Point(17, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(120, 19);
            this.label2.TabIndex = 10;
            this.label2.Text = "Plate Number";
            // 
            // txtDriverName
            // 
            this.txtDriverName.Location = new System.Drawing.Point(22, 36);
            this.txtDriverName.Name = "txtDriverName";
            this.txtDriverName.Size = new System.Drawing.Size(260, 20);
            this.txtDriverName.TabIndex = 9;
            this.txtDriverName.CharacterCasing = System.Windows.Forms.CharacterCasing.Upper;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Cooper Black", 12F);
            this.label1.ForeColor = System.Drawing.Color.Black;
            this.label1.Location = new System.Drawing.Point(16, 14);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(113, 19);
            this.label1.TabIndex = 8;
            this.label1.Text = "Driver Name";
            // 
            // FormDriverInfo
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Black;
            this.ClientSize = new System.Drawing.Size(300, 217);
            this.Controls.Add(this.panel1);
            this.DoubleBuffered = true;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "FormDriverInfo";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.FormDriverInfo_Load);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private btnProduct.Button_WOC btnDInfoConfirm;
        private System.Windows.Forms.TextBox txtPlateNo;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtDriverName;
        private System.Windows.Forms.Label label1;
        private btnProduct.Button_WOC btnDInfoCancel;
        private System.Windows.Forms.Label lblType;
        
    }
}